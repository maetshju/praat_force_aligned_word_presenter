#' MALD Alignment Checking Tool
#' 
#' Copyright Matthew C. Kelley, July 2016
#'
#' This software is licensed under the GPLv2. For more information, please
#' visit [the GNU website](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html).
#'
#' This script is to facilitate iterating through a list of files passed through
#' a forced aligner and marking which ones have good alignment
#' and which ones should be re-examined, while also allowing the user to make
#' any necessary changes to the TextGrid.
#'
#' **Requires:**
#' 
#' * WAV files to be checked in a single directory
#' 
#' * TextGrid files to accompany the WAV files (these MUST have the same name
#' as the WAV files)
#'
#' @param directory The absolute directory in which the WAV and TextGrid files
#' reside.
#' 
#' @param results The absolute name of the file to which the results will be
#' written.
#'
#' @param starting The word to start from in the list. The script will iterate
#' throug the different files (using the filename as the word) until it reaches
#' this specified one.
#'
#' @param writehead Boolean value whether to write the header to the file or
#' not. This is helpful when starting a new file, but not when continuing an old
#' file, so this is off by default.
#'
#' @calc results The file to which the results are written to, which is
#' formatted in a tab-delimited text format.

# Create a form to collect information to start with
form MALD Alignment Checking Tool
	comment This script presents all the words in a directory and lets you select whether the alignment is good or needs further examination

	comment Full path to the directory:
	text directory

	comment Full path name of file to write results to:
	text results

	comment Word to start from (leave blank to start from beginning)
	text starting

	comment Write header?
	boolean writehead no
endform

# Check if we're starting from the first word or not
if starting$ = ""
	reachedStarting = 1
else
	reachedStarting = 0
endif

# Write header if specified
if writehead
	header$ = "Word	Looks good	Notes'newline$'"
	fileappend 'results$' 'header$'
endif

# Variable to hold the slash direction, since the script
# doesn't seem to work when passed a "\" as a string literal
slash$ = "\"

# Get a list of all the files in the specified directory
Create Strings as file list... list 'directory$''slash$'*.wav
	Replace all... .wav "" 0 Literals
	Rename... listnew
	select Strings list
	Remove
	select Strings listnew

	numberOfFiles = Get number of strings
	
	# Get to the specified starting word
	i = 1
	starting$ = replace_regex$(starting$,  "[A-Z]", "\L&", 0)
	while reachedStarting = 0
		if i > numberOfFiles
			select Strings listnew
			Remove
			exitScript: "The specified starting word ", starting$, " was not found. Please check your spelling and try again."
		endif
		soundname$ = Get string... i
		soundname$ = replace_regex$(soundname$, "[A-Z]", "\L&", 0)
		if soundname$ = starting$
			reachedStarting = 1
		else
			i = i + 1
		endif
	endwhile

	# Walk through the WAV files
	for ifile from i to numberOfFiles

		# Get and open the current sound file and TextGrid
		soundname$ = Get string... ifile
		Read from file... 'directory$''slash$''soundname$'.wav
		sound$ = selected$("Sound",1)

		Read from file... 'directory$''slash$''soundname$'.TextGrid
		grid$ = selected$("TextGrid",1)

		selectObject: "TextGrid 'grid$'"
		numTiers = Get number of tiers
		if numTiers = 2
			Duplicate tier: 1, 1, "phone"
		endif

		plusObject: "Sound 'sound$'"
		View & Edit

		mynotes$ = ""
		# Ask if alignment is good, and allow the user to save changes to the TextGrid
		# with the "Apply changes" button
		clicked = 3
		while clicked = 3
			beginPause: "Alignment Evaluation"
				comment: "You're on word number " + string$(ifile) + "."
				comment: "Does the alignment look okay for this word?"
				text: "notes", "'mynotes$'"
			clicked = endPause: "Yes", "No", "Apply changes", "End", 1, 4
			# Apply changes and start loop over
			if clicked = 3
				select TextGrid 'grid$'
				Save as text file... 'directory$''slash$''soundname$'.TextGrid
				mynotes$ = notes$
			endif
		endwhile

		# Print state when the script is told to end
		if clicked = 4
			writeInfoLine: "You've left off on word 'sound$',  which is word 'ifile'."
			appendInfoLine: "Start from that word when you begin checking again."

			editor: "TextGrid 'grid$'"
			Close

			select Sound 'sound$'
			Remove

			select TextGrid 'grid$'
			Remove

			select Strings listnew
			Remove

			exitScript: ""
		elsif clicked = 1
			status$ = "yes"
		else
			status$ = "no"
		endif

############
## PRINT  ##
############

resultline$ = "'sound$'	'status$'	'notes$''newline$'"		
fileappend 'results$' 'resultline$'

		editor: "TextGrid 'grid$'"
		Close
		select Sound 'sound$'
		Remove
		select TextGrid 'grid$'
		Remove
		select Strings listnew
	endfor
select Strings listnew
Remove
