# Praat Force-Aligned Word Presenter

Developed for use in pre-processing stimuli for experiments in the [Alberta Phonetics Laboratory](https://aphl.artsrn.ualberta.ca/), this Praat script will present all WAV and TextGrid pairs in a directory that have been passed through a forced aligner tool. This allows a user to more quickly and regularly check the alignment of their files, and stores the results of the alignment in a file as well for later reference.

**Proceed to the GitLab repository** [here](https://gitlab.com/maetshju/praat_force_aligned_word_presenter/), or find the documentation in the menu.

This script is licensed under [version 2 of the GNU General Public License](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html).
