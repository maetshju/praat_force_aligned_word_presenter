# present_words.praat

MALD Alignment Checking Tool

Copyright Matthew C. Kelley, July 2016

This software is licensed under the GPLv2. For more information, please visit [the GNU website](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html).

This script is to facilitate iterating through a list of files passed through a forced aligner and marking which ones have good alignment and which ones should be re-examined, while also allowing the user to make any necessary changes to the TextGrid.

**Requires:**

* WAV files to be checked in a single directory

* TextGrid files to accompany the WAV files (these MUST have the same name as the WAV files)

## Arguments to the Script

* *directory* The absolute directory in which the WAV and TextGrid files reside.

* *results* The absolute name of the file to which the results will be written.

* *starting* The word to start from in the list. The script will iterate throug the different files (using the filename as the word) until it reaches this specified one.

* *writehead* Boolean value whether to write the header to the file or not. This is helpful when starting a new file, but not when continuing an old file, so this is off by default.

## Script Calculations

* *results* The file to which the results are written to, which is formatted in a tab-delimited text format.
