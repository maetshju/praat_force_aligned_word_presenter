# Running the Script

This script, as with all Praat scripts, must be run using the [Praat](http://www.praat.org) program.

## Loading the Script in Praat

To begin, you must open the script in Praat:

1. Open Praat.
2. Click on the `Open` menu in Praat Objects menu.
3. Click on the `Read from file...` option.
4. Navigate to where you have saved the script and open it. A Script Editor window will open.

## Executing the Script

To execute the script:

1. Click on the `Run` menu in the Script Editor window.
2. Click on the `Run` option. The form that take initial arguments will appear.
3. Fill in the fields as appropriate (see generated documentation for details).
4. Click the `OK` button.

## Using the Features

The script will iterate through each WAV and TextGrid file pair present in the directory that has been passed to the script in the initial form. Each pair will be shown in a `TextGrid` Editor window.

As each pair is opened, a duplicate of the top tier, which is assumed to be your phone tier, will be created so that non-destructive editting can take place.

Make any changes that you would like to make, then click the `Apply changes` button, which will save the modified `TextGrid`. **Note: if you do not click this button, your changes will not be saved!**

Fill in any comments you may have in the textbox, and then click the `Yes` or `No` button as appropriate to the question of whether the alignment looked okay to you or not.

When you're done, click on the `End` button, which will display a window telling you which word you're on, and the associated word number (which is for user convenience only, as the script does not make sue of that information). You may start from where you left off if you feed the word back into the script when you start it up again.
